package com.example.demogitcm.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MainController {

    @GetMapping(path = "/api/hello")
    public String getMessage() {
        return "hello world";
    }

    @GetMapping(path = "/api/aa")
    public String abc() {
        return "this is test";
    }

}
